# MasterPraktikumMLinCrowdSim4

Python 3.7 is needed and Poetry:

https://python-poetry.org/docs/
```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

## Install the dependencies
```bash
poetry install
```
Using jupyter notebook is recommended.