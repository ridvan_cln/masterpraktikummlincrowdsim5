import numpy as np
import random
from sklearn.metrics.pairwise import euclidean_distances

# Returns A
def least_squares(X,b):
    """Calculate the least_squares of X and b

    Args:
        X (Matrix): A Matrix.
        b (Matrix): A Matrix.

    Returns:
        Matrix: Least_squares Matrix.
    """    
    #return np.dot(np.linalg.inv(np.dot(X.T, X)), np.dot(X.T, F))
    return np.linalg.lstsq(X, b, rcond=None)[0].T

def rbf(X,K,epsilon):
    """Radial basis function as defined in the lecutre.

    Args:
        X (Matrix): A Matrix
        K (int): Number of radial basis functions.
        epsilon (float): Tolerance.

    Returns:
        Matrix: Return a Matrix calc. with Radial Basis Functions.
    """    
    K = K # number of centers for RBF
    indices=np.random.choice(a=len(X[:,0]),size=K) # choose numbers from 0 to D^(1)
    subsampled_data_points=X[indices,:] # M_sub x D
    epsilon = 1

    beta = 0.5*np.power(1.0/epsilon,2)
    Kern = np.exp(-beta*euclidean_distances(X=X, Y=subsampled_data_points,squared=True))
    return Kern

if __name__ == "__main__":
    # execute only if run as a script
    pass