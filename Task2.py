import numpy as np
#from scipy import integrate
#import sympy as sm
import matplotlib.pyplot as plt

def finite_diff(X0, X1, delta_t=1):
    X = np.subtract(X0, X1)
    X = X / delta_t
    return X

def error_estimation(X_hat, X):
    mse = np.square(np.subtract(X_hat, X)).mean()/X.shape[0] 
    return mse


def plot_phase_portrait_2D(fig, X, A, color, x_min=-10, x_max=10, y_min=-10, y_max=+10):
    x, y = X.T

    x_mesh = np.linspace(x_min, x_max, 20)
    y_mesh = np.linspace(y_min, y_max, 20)
    arr = np.empty((20,2))
    X1 , Y1  = np.meshgrid(x_mesh, y_mesh) # create a grid

    DX1, DY1 = np.dot(A-np.eye(2), [X1.ravel(), Y1.ravel()])

    M = (np.hypot(DX1, DY1))               # norm growth rate 
    M[ M == 0] = 1.                        # avoid zero division errors 
    DX1 /= M                               # normalize each arrows
    DY1 /= M

    axes = plt.gca()
    axes.set_xlim([x_min,x_max])
    axes.set_ylim([y_min,y_max])

    plt.quiver(X1, Y1, DX1, DY1, M, pivot='mid')
    plt.plot(x, y, color=color)
    plt.title("Phaseportrait")

    
def plot_phase_2D(X, color):
    x, y = X.T
    plt.plot(x,y, color=color)
    plt.title("Phase space")




