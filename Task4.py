import numpy as np
from scipy.integrate import solve_ivp

# Lorenz paramters and initial conditions
sigma, beta, rho = 10, 2.667, 28
u0, v0, w0 = 0, 1, 1.05

# Maximum time point and total number of time points
tmax = 100

# Approx. of Lorenz Vektorfield
C = None

def lorenz(t, X):
    """The Lorenz equations."""
    u, v, w = X
    up = -sigma*(u - v)
    vp = rho*u - v - u*w
    wp = -beta*w + u*v
    return up, vp, wp

def approx_lorenz(t, X):
    """ Bonus points, but not finished"""
    return C@X.T

def solve_ivp_of_func(func, n):
    # Integrate the Lorenz equations on the time grid t
    t = np.linspace(0, tmax, n)
    args = ()
    f = solve_ivp(globals()[func], [0, tmax], (u0, v0, w0), t_eval=t, method='DOP853')#, args=(sigma, beta, rho))
    return f.y

def roll_lorenz(x, dt):
    """time delay with np.roll

    Args:
        x (array): Datapoints
        dt (delay): how often the array has to be shifted

    Returns:
        array: shifted array
    """    
    x_t = np.roll(x,dt)
    x_t_pdt = np.roll(x,2*dt)
    x_t_p2dt = np.roll(x,3*dt)
    return x_t,x_t_pdt,x_t_p2dt